﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LabOP1
{
    class Matrix
    {
        double[,] matr;

        public Matrix(double[,] matr)
        {
            this.matr = matr;
        }
        public Matrix(Matrix matr1)
        {
             this.matr = matr1.matr;
        }
        public Matrix(int a, int b)
        {
            matr = new double[a, b]; 
        }

        public void RandMatrix()
        {
            Random rand = new Random();
            for (int i = 0; i < this.matr.GetLength(0); i++)
            {
                for (int j = 0; j < this.matr.GetLength(1); j++)
                {
                    this.matr[i, j] = rand.Next(-10, 10);
                }
            }
        }

        public void PrintMatrix()
        {
            for (int i = 0; i < this.matr.GetLength(0); i++)
            {
                for (int j = 0; j < this.matr.GetLength(1); j++)
                {
                     Console.Write("[{0,6}]",this.matr[i, j]);
                }
                Console.WriteLine();
            }
            Console.WriteLine();
        }

        public static Matrix operator *(Matrix a,Matrix b) 
        {
            if (a.matr.GetLength(1) != b.matr.GetLength(0)) 
        	 {
             Console.WriteLine("Такое умножение невозможно, количество столбцов первой матрицы не равна количеству строк второй матрицы.");
                return a;
	         }
                else
             {
             Matrix mat = new Matrix(a.matr.GetLength(0),b.matr.GetLength(1));
                for (int i = 0; i < a.matr.GetLength(0); i++)
                {
                    for (int j = 0; j < b.matr.GetLength(1); j++)
                    {
                        for (int k = 0; k < b.matr.GetLength(0); k++)
                        {
                            mat.matr[i, j] += a.matr[i,k] * b.matr[k,j];

                        }
                    }
                }
                return mat;
             }
        }

        public double Determinant()
        {
            double result = Deter(matr);
            return result;
        }

        public double Deter(double[,] mat)
        {
            double result = 0;
            if (mat.GetLength(0) == 2)
            {
                result = mat[0, 0] * mat[1, 1] - mat[0, 1] * mat[1, 0];
            }
            else
            {
                for (int i = 0; i < mat.GetLength(1); i++)
                {
                    double[,] mat2 = new double[mat.GetLength(1) - 1, mat.GetLength(1) - 1];
                    int q = 0;
                    for (int j = 0; j < mat.GetLength(1)-1; j++)
                    {
                        q = 0;
                        for (int k = 0; k < mat.GetLength(1)-1; k++)
                        {
                            if (q == i) q++;
                            mat2[j, k] = mat[j+1, q];
                            q++;
                        }
                    }

                    result += (int)Math.Pow(-1, i) * mat[0,i] * Deter(mat2);
                }
            }
            return result;
        }
        public void Inverse()
        {
          matr = Accord.Math.Matrix.Inverse(matr);
            for (int i = 0; i < matr.GetLength(0); i++)
            {
                for (int j = 0; j < matr.GetLength(1); j++)
                {
                    matr[i, j] = Math.Round(matr[i, j], 3);
                }
            }
        }

        public void Transposed()
        {
            Matrix demo = new Matrix(this.matr.GetLength(1), this.matr.GetLength(0));
            for (int i = 0; i < this.matr.GetLength(1); i++)
            {
                for (int j = 0; j < this.matr.GetLength(0); j++)
                {
                    demo.matr[i, j] = this.matr[j, i];
                }
            }
            this.matr = demo.matr;
        }

    }
}
