﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPLab3
{
   
    static class Class
    {
        public delegate void DelCalculate(string message);
        static public event DelCalculate Info;
        static public double calculate(double x)
        {
            var a = x / 5.0;
            Console.WriteLine(a);
            if (Info != null)
                Info("First section is ready");
            return (a);
        }

        static public double calculateY(double y)
        {
            var a = Math.Pow(y, 2) + 5;
            Console.WriteLine(a);
            if (Info != null)
                Info("Second section is ready");
            return a;
        }
    }

    static class Demo
    {
        public static double calculate2(double x)
        {
            var a = Math.Pow(Math.E, x);
            Console.WriteLine(a);
            return a;
        }
    }
}
