﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2
{
    class Program
    {
        static void Main(string[] args)
        {
            Cars Car = new Cars("Ford");
            Car.CarMessage += Show_Message;
            Car.Print();
                Console.Write("Write a new name please:");
                string name = Console.ReadLine();
                Console.WriteLine("------{Menu}------");
                Console.WriteLine("1.Change car name");
                Console.WriteLine("2.Not change car name");
                Console.WriteLine("-------------------");
                int input = int.Parse(Console.ReadLine());
                switch (input)
                {
                    case 1:
                        Car.ChangeCar(name);
                        break;
                    case 2: Car.Error(); return;
                }
        }

        public static void Show_Message(string text)
        {
            Console.WriteLine(text);
        }
    }
}
