﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task3
{
    public delegate void TrainMessage(object sender, TrainEventArgs e);
    public class Train
    {
        public event TrainMessage Message;
        public string Name;
        public string Time;
        public int Number;
        public string Wagon;
        public int Place;
        public Train()
        {
        }
        public Train(string name, string time, int number, string wagon, int place)
        {
            Name = name;
            Time = time;
            Number = number;
            Wagon = wagon;
            Place = place;
        }

        public void Add(Train train)
        {
            Name = train.Name;
            Time = train.Time;
            Number = train.Number;
            Wagon = train.Wagon;
            Place = train.Place;

            Message?.Invoke(this, new TrainEventArgs(this, $"The train was added\n{this}"));
        }
        public override string ToString()
        {
            return $"************************\nTrain name: {Name,-10}\nFinish time: {Time}\n" +
                $"Number: {Number}\nWagon: {Wagon}\nPlace: {Place}\n************************";
        }

        public void ChangeName(string name)
        {
            if (this.Name != null && this.Time != null && this.Number != 0 && this.Wagon != null && this.Place != 0)
            {
                Name = name;
                if (Message != null)
                    Message(this, new TrainEventArgs(this, $"Name changed!\n{this}"));
            }
            else Message(this, new TrainEventArgs(this, $"Ticket is empty"));
        }
        public void ChangeTime(string time)
        {
            if (this.Name != null && this.Time != null && this.Number != 0 && this.Wagon != null && this.Place != 0)
            {
                Time = time;
                if (Message != null)
                    Message(this, new TrainEventArgs(this, $"Time changed!\n{this}"));
            }
            else Message(this, new TrainEventArgs(this, $"Ticket is empty"));
        }
        public void ChangeNumber(int number)
        {
            if (this.Name != null && this.Time != null && this.Number != 0 && this.Wagon != null && this.Place != 0)
            {
                Number = number;
                if (Message != null)
                    Message(this, new TrainEventArgs(this, $"Number changed!\n{this}"));
            }
            else Message(this, new TrainEventArgs(this, $"Ticket is empty"));
        }
        public void ChangeWagon(string wagon)
        {
            if (this.Name != null && this.Time != null && this.Number != 0 && this.Wagon != null && this.Place != 0)
            {
                Wagon = wagon;
                if (Message != null)
                    Message(this, new TrainEventArgs(this, $"Wagon changed!\n{this}"));
            }
            else Message(this, new TrainEventArgs(this, $"Ticket is empty"));
        }
        public void ChangePlace(int place)
        {
            if (this.Name != null && this.Time != null && this.Number != 0 && this.Wagon != null && this.Place != 0)
            {
                Place = place;
                if (Message != null)
                    Message(this, new TrainEventArgs(this, $"Place changed!\n{this}"));
            }
            else Message(this, new TrainEventArgs(this, $"Ticket is empty"));
        }

        public void DeleteAll()
        {
            if (this.Name != null && this.Time != null && this.Number != 0 && this.Wagon != null && this.Place != 0) 
            {
                Name = null ;
                Time = null;
                Number = 0;
                Wagon = null;
                Place = 0;

                Message?.Invoke(this, new TrainEventArgs(this, $"Train info is deleted!\n"));
            }
            else
            Message?.Invoke(this, new TrainEventArgs(this, $"Ticket is empty\n"));
        }

        public void Print() => Console.WriteLine(this.ToString());

        public void Code()
        {
            if (this != null)
            {
                var rand = new Random();
                int code = rand.Next(10000, 99999);
                if (Message != null)
                    Message(this, new TrainEventArgs(this, $"Payment code: {code}"));
            }
            else
                if (Message != null)
                Message(this, new TrainEventArgs(this, $"Payment code not created"));
        }
    }
    public class TrainEventArgs : EventArgs
    {
        public string Name;
        public string Time;
        public int Number;
        public string Wagon;
        public int Place;
        public string Message;
        public TrainEventArgs(string name, string time, int number, string wagon, int place, string message)
        {
            Name = name;
            Time = time;
            Number = number;
            Wagon = wagon;
            Place = place;
            Message = message;
        }

        public TrainEventArgs(Train train, string message)
        {
            Name = train.Name;
            Time = train.Time;
            Number = train.Number;
            Wagon = train.Wagon;
            Place = train.Place;
            Message = message;
        }

    }

}
