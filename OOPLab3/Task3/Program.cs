﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task3
{
    class Program
    {
        static Train train = new Train();
        static void Main(string[] args)
        {
            train.Message += Show_Message;
            for (;;)
            {
                Console.WriteLine("--------{Menu}--------");
                Console.WriteLine("1.Create new ticket");
                Console.WriteLine("2.Change name");
                Console.WriteLine("3.Change finish time");
                Console.WriteLine("4.Change Number");
                Console.WriteLine("5.Change Wagon");
                Console.WriteLine("6.Change Place");
                Console.WriteLine("7.Delete info");
                Console.WriteLine("8.Print info");
                Console.WriteLine("9.Create payment code");
                Console.WriteLine("10.Exclude events");
                Console.WriteLine("11.Include events");
                Console.WriteLine("12.Exit");
                Console.WriteLine("-----------------------");
                int input = int.Parse(Console.ReadLine());

                switch (input)
                {
                    case 1:
                        {
                            Console.WriteLine("Enter name:");
                            string name = Console.ReadLine();
                            Console.WriteLine("Enter time:");
                            string time = Console.ReadLine();
                            Console.WriteLine("Enter number");
                            int number = int.Parse(Console.ReadLine());
                            Console.WriteLine("Enter wagon");
                            string wagon = Console.ReadLine();
                            Console.WriteLine("Enter place");
                            int place = int.Parse(Console.ReadLine());
                            train.Add(new Train(name, time, number, wagon, place));
                        }
                        break;
                    case 2:
                        {
                            Console.WriteLine("Enter name:");
                            string name = Console.ReadLine();
                            train.ChangeName(name);
                        }
                        break;
                    case 3:
                        {
                            Console.WriteLine("Enter time:");
                            string name = Console.ReadLine();
                            train.ChangeTime(name);
                        }
                        break;
                    case 4:
                        {
                            Console.WriteLine("Enter number:");
                            int number = int.Parse(Console.ReadLine());
                            train.ChangeNumber(number);
                        }
                        break;
                    case 5:
                        {
                            Console.WriteLine("Enter wagon:");
                            string wagon = Console.ReadLine();
                            train.ChangeWagon(wagon);
                        }
                        break;
                    case 6:
                        {
                            Console.WriteLine("Enter place:");
                            int place = int.Parse(Console.ReadLine());
                            train.ChangePlace(place);
                        }
                        break;
                    case 7: train.DeleteAll(); break;
                    case 8: train.Print(); break;
                    case 9: train.Code(); break;
                    case 10:train.Message -= Show_Message; break;
                    case 11:train.Message += Show_Message; break;
                    case 12: return;
                }
            }
           void Show_Message(object sender, TrainEventArgs e) => Console.WriteLine(e.Message);
    }
}

       
}
