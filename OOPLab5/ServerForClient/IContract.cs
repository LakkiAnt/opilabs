﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;


namespace ServerForClient
{
    [ServiceContract]


    interface IContract
    {
        [OperationContract]
        void Say(string text, string name);

        [OperationContract]
        void Connect(string text);

        [OperationContract]
        void Disconnect(string text);

        [OperationContract]
        List<string> GetProcess();

        [OperationContract]
        void OpenProcess(string text);

        [OperationContract]
        void CloseProcess(string text);
        [OperationContract]
        Bitmap TakeScreen();
        [OperationContract]
        void ComputerOff();
        [OperationContract]
        void ComputerReload();
        [OperationContract]
        void ComputerSuspend();
    }
}
