﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace OOPLab2
{
    public partial class Form1 : Form
    {
        delegate double mydel();
        mydel delcalc;
        public Form1()
        {
            InitializeComponent();
            delcalc = calculate;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            var timer = new Stopwatch();
            timer.Start();
            var index = delcalc.BeginInvoke(null,null);
            button1.Enabled = false;
            var result = delcalc.EndInvoke(index);
            button1.Enabled = true;
            timer.Stop();
            textBox1.Text = result.ToString();
            textBox2.Text = timer.ElapsedMilliseconds.ToString() + " ms";
        }

        public static double calculate()
        {
            double start = -100000, finish = 10000, step = 0.015, result = 0;
            for (double i = start; i < finish; i += step)
            {
                result += 4 * Math.Pow(i, 3) - 7 * Math.Pow(i, 2) + 1;
            }
            return result;
        }
    }
}
