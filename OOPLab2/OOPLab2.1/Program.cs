﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPLab2._1
{
    class Program
    {
        static Library libr;
        static void Main(string[] args)
        {
            libr = new Library();
            libr.Books.Add(new Book("Керри", "Стивен", "Мкт"));
            libr.Books.Add(new Book("Майстер", "Михайло", "Свр"));
            libr.Books.Add(new Book("Мартин Иден", "Джек", "Лнд"));
            libr.Books.Add(new Book("Скотний двир", "Джордж", "Мкт"));
            libr.Books.Add(new Book("О дивний новий свит", "Хакси", "Аон"));

            libr.PrintAll();
            Console.WriteLine();
            Console.WriteLine($"1 - Сортувати за назвою");
            Console.WriteLine($"2 - Сортувати за автором");
            Console.WriteLine($"3 - Сортувати за видавництвом");
            Console.WriteLine();
            int input = int.Parse(Console.ReadLine());
            Sort(input);
            libr.PrintAll();
        }

        public static CompareDel Choose(int i)
        {
            switch (i)
            {
                case 1: return (Book first, Book second) => first.Name.CompareTo(second.Name);
                case 2: return (Book first, Book second) => first.Auhtor.CompareTo(second.Auhtor);
                case 3: return (Book first, Book second) => first.Publics.CompareTo(second.Publics);
            }
            return null;
        }

        public static void Sort(int i)
        {
            libr.compare = Choose(i);
            libr.Sort();
        }
    }
}
