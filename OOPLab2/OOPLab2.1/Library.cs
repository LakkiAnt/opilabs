﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPLab2
{
    delegate int CompareDel(Book a, Book b);
    class Library
    {
        public CompareDel compare;
        public List<Book> Books { get; set; }

        public Library()
        {
            Books = new List<Book>();
        }
        public Library(List<Book> books)
        {
            Books.Concat(books);
        }

        public void Sort()
        {
            for (int i = 0; i < Books.Count; i++)
            {
                for (int j = 0; j < Books.Count - 1 - i; j++)
                {
                    if (compare(Books[j],Books[j+1]) == 1)
                    {
                        var demo = Books[j];
                        Books[j] = Books[j + 1];
                        Books[j + 1] = demo;
                    }
                }
            }
        }

        public void PrintAll()
        {
            foreach (var item in Books)
            {
                Console.WriteLine($"{Books.IndexOf(item)}){item}");
            }
        }

    }
}
