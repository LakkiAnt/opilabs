﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Security.Cryptography;
using System.IO;

namespace OOPLab4
{
    class FileEditor
    {
        public List<FileInfo> obj { get; set; }
        public static long Size;

        public FileEditor()
        {
            obj = new List<FileInfo>();
        }

        public void Addfiles(OpenFileDialog files)
        {
            obj.Clear();
            obj.Add(new FileInfo(files.FileNames[0]));
        }

        public long GetSize() { return Size; }

        public static void EncryptData(SymmetricAlgorithm aesAlgorithm, string text, string fileName)
        {
            var encryptor = aesAlgorithm.CreateEncryptor(aesAlgorithm.Key, aesAlgorithm.IV);
            using (var ms = new MemoryStream())
            {
                using (var cs = new CryptoStream(ms, encryptor, CryptoStreamMode.Write))
                using (var writer = new StreamWriter(cs))
                    writer.Write(text);

                byte[] encryptedDataBuffer = ms.ToArray();
                File.WriteAllBytes(fileName + ".crypt", encryptedDataBuffer);
            }
            Size = new System.IO.FileInfo(fileName + ".crypt").Length;
        }

        public static void DecryptData(SymmetricAlgorithm aesAlgorithm, string fileName)
        {
            var decryptor = aesAlgorithm.CreateDecryptor(aesAlgorithm.Key, aesAlgorithm.IV);
            byte[] encryptedDataBuffer = File.ReadAllBytes(fileName);
            string text = "";
            using (var ms = new MemoryStream(encryptedDataBuffer))
            using (var cs = new CryptoStream(ms, decryptor, CryptoStreamMode.Read))
            using (var reader = new StreamReader(cs))
                text = reader.ReadToEnd();
            using (var writer = new StreamWriter(Path.ChangeExtension(fileName, null)))
                writer.Write(text);
        }
    }
}
