﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace Task2
{
    public partial class Form1 : Form
    {
        Process proc;
        List<string> work = new List<string>();
        public Form1()
        {
            InitializeComponent();
            comboBox1.SelectedIndex = 0;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            PrintList();
        }

        public void UpdateInfo()
        {
            proc = Process.GetProcessesByName(listBox1.SelectedItem.ToString())[0];
            ProcessThreadCollection processThreads = proc.Threads;
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            textBox4.Text = "";
            textBox5.Text = "";
            textBox6.Text = "";
            try
            {
                textBox1.Text = proc.VirtualMemorySize64.ToString();
                textBox2.Text = proc.StartTime.ToString();
                textBox3.Text = processThreads[0].Id.ToString();
                textBox4.Text = proc.ProcessName.ToString();
                textBox5.Text = proc.Handle.ToString();
                textBox6.Text = processThreads[0].CurrentPriority.ToString();
            }
            catch
            {
                MessageBox.Show("До деяких даних доступ заблоковано!");
            }
        }


        private void button1_Click(object sender, EventArgs e)
        {
            Process.Start("calc");
            PrintList();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Process.Start("WINWORD");
            PrintList();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Process.Start("notepad");
            PrintList();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Process.Start("vlc");
            PrintList();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Process.Start("mspaint");
            PrintList();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            proc.Kill();
            textBox7.Text = "";
            PrintList();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateInfo();
        }

        private void textBox7_TextChanged(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            PrintList();
            if (listBox1.SelectedItem != null) UpdateInfo();
        }

        public void PrintList()
        {
            foreach (var process in Process.GetProcesses())
            {
                if (process.ProcessName.StartsWith(textBox7.Text) || textBox7.Text == "")
                {
                    listBox1.Items.Add(process.ProcessName);
                }
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
          

        }

        private void button7_Click(object sender, EventArgs e)
        {
            switch (comboBox1.SelectedIndex)
            {
                case 0: proc.PriorityClass = ProcessPriorityClass.Idle; break;
                case 1: proc.PriorityClass = ProcessPriorityClass.Normal; break;
                case 2: proc.PriorityClass = ProcessPriorityClass.BelowNormal; break;
                case 3: proc.PriorityClass = ProcessPriorityClass.High; break;
                case 4: proc.PriorityClass = ProcessPriorityClass.RealTime; break;
            }
            UpdateInfo();
          
        }
    }
}
